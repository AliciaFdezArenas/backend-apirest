package com.techu.apirest.controller;
import com.techu.apirest.model.ProductoModel;
import com.techu.apirest.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List; import java.util.Optional;
@RestController @RequestMapping("/apitechu/v2")
@CrossOrigin(origins ="*",methods = {RequestMethod.GET, RequestMethod.POST})
public class ProductoController {
    @Autowired
    ProductoService productoService;

    @GetMapping("/productos")
    public List<ProductoModel> getProductos() {
        return productoService.findAll();
    }

    @GetMapping("/productos/{id}" )
    public Optional<ProductoModel> getProductoId(@PathVariable String id){
        return productoService.findById(id);
    }

    @PostMapping("/productos")
    public ProductoModel postProductos(@RequestBody ProductoModel newProducto){
        productoService.save(newProducto);
        return newProducto;
    }
    /*
    @PutMapping("/productos")
    public void putProductos(@RequestBody ProductoModel productoToUpdate){
        productoService.save(productoToUpdate);
    }*/
    @PutMapping("/productos")
    public boolean putProductos(@RequestBody ProductoModel productoToUpdate){
        if (productoService.existsByID(productoToUpdate.getId())){
            productoService.save(productoToUpdate);
            return true;
        } else {
            return false;
        }
    }
    @PutMapping("/productos/{id}")
    public void putProductosById(@RequestBody ProductoModel productoToUpdate, @PathVariable String id){
            productoService.saveById(productoToUpdate,id);
    }

    @DeleteMapping("/productos")
    public boolean deleteProductos(@RequestBody ProductoModel productoToDelete){
        return productoService.delete(productoToDelete);
    }

    @DeleteMapping("/productos/{id}")
    public boolean deleteProductosById(@PathVariable String id) {
        return productoService.deleteById(id);
    }

    @PatchMapping("productos/{precio}")
    public boolean patchProductos(@RequestBody ProductoModel productoToPatch, @PathVariable Double precio){
        if(productoService.findById(productoToPatch.getId()).isPresent()) {
                productoToPatch.setPrecio(precio);
                productoService.save(productoToPatch);
                return true;
        }
        return false;
    }

}

