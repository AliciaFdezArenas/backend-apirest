package com.techu.apirest;
import com.techu.apirest.controller.HolaMundoController;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.assertEquals;



public class HolaMundoControllerTest {

    @Test
    public void testCaseHM() {
        HolaMundoController HMController = new HolaMundoController();
        assertEquals("Hola CORS", HMController.saludar());
    }
}